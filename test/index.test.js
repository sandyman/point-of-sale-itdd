const chai = require('chai');
const Display = require('../display');
const PointOfSale = require('../index');

const expect = chai.expect;

describe('Point of Sale', () => {
    const pos = new PointOfSale();

    it('should accept events from the bar code scanner through event handler', () => {
        expect(pos.onBarCode).to.be.a('function');
    });

    it('should display correct price for product we know', () => {
        // Jbrains's bar code
        pos.onBarCode('063491028120');

        // Jbrains's suggested price
        expect(pos.lastTextDisplayed()).to.equal('$11.50');
    });

    it('should not display anything when bar code is empty', () => {
        pos.onBarCode('');
        expect(pos.lastTextDisplayed()).to.equal('');
    });

    it('should not display anything when bar code is undefined', () => {
        pos.onBarCode();
        expect(pos.lastTextDisplayed()).to.equal('');
    });

    it('should display a message when bar code is not empty, but unknown', () => {
        pos.onBarCode('123456789012');
        expect(pos.lastTextDisplayed()).to.equal('Unknown product');
    });

    it('should display correct price when another known product\'s bar code is scanned', () => {
        pos.onBarCode('978063407726');
        expect(pos.lastTextDisplayed()).to.equal('$9.98');
    });
});

describe('Display', () => {
    it('should accept display texts for event handler', () => {
        expect((new Display()).onDisplay).to.be.a('function');
    });
});
