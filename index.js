const Display = require('./display');

class PointOfSale {
    constructor() {
        this.display = new Display();
        this.productPrices = {
            '063491028120': '$11.50',
            '978063407726': '$9.98',
        };
        this._lastTextDisplayed = '';
    }

    onBarCode(barCode) {
        let textToDisplay = 'Unknown product';
        if (!barCode || barCode === '') {
            textToDisplay = '';
        } else if (barCode in this.productPrices) {
            textToDisplay = this.productPrices[barCode];
        }
        this.display.onDisplay(textToDisplay);
        this._lastTextDisplayed = textToDisplay;
    }

    lastTextDisplayed() {
        return this._lastTextDisplayed;
    }
}

module.exports = PointOfSale;
